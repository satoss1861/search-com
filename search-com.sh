#!/bin/bash
# Script de recherche de données dans asterisk
#
# v2021O630 - MTI - Creation du script
#
# (c) 2021 HEXATEL (wwww.hexatel.fr)
#

recherche_destination(){
echo "   fichier     | date d'appel          |   numero de com    |   destination de l'appel  |"
for i in `zgrep XIVO_SRCNUM=$1 /var/log/asterisk/$2 | awk -F"[" '{print $4}'| awk -F"]" '{print $1}'`; do
    retour=$(zgrep $i /var/log/asterisk/$2 | zgrep --color=auto 'XIVO_DSTNUM=')
        fichier=$(echo -n $retour |awk -F":" '{print $1}')
#       if [[ $fichier == /* ]]
#       then
#               fichier=$(echo -n $retour |awk -F":" '{print $1}')
#       else
#               fichier=/var/log/asterisk/$2
#       fi
        date_search=$(echo -n $retour |awk -F"[" '{print $2}'| awk -F"]" '{print $1}')
        numero_comm=$(echo -n $retour |awk -F"[" '{print $4}'| awk -F"]" '{print $1}')
        xivo_dstnum=$(echo -n $retour |awk -F"\"" '{print $4}')

        echo $fichier  "   |    "  $date_search "|     " $numero_comm   "   |"   $xivo_dstnum "       |"
done
}

recherche_source(){
# affiche la date et l'heure
echo "   fichier     | date d'appel          |   numero de com    |   destination de l'appel  |"
for i in `zgrep XIVO_DSTNUM=$1 /var/log/asterisk/$2 | awk -F"[" '{print $4}'| awk -F"]" '{print $1}'`; do
      retour=$(zgrep $i /var/log/asterisk/$2 | zgrep --color=auto 'XIVO_SRCNUM=')
          fichier=$(echo -n $retour |awk -F":" '{print $1}')
          date_search=$(echo -n $retour |awk -F"[" '{print $2}'| awk -F"]" '{print $1}')
          numero_comm=$(echo -n $retour |awk -F"[" '{print $4}'| awk -F"]" '{print $1}')
          xivo_srcnum=$(echo -n $retour |awk -F"\"" '{print $4}')

          echo $fichier "   |    "   $date_search "|     " $numero_comm   "   |"   $xivo_srcnum "       |"
done
}

affiche_com(){
        zgrep --color=auto $1 /var/log/asterisk/$2
}

usage()
{
   # Display Help
   echo "fonctions de recherche de numéros dans les logs"
   echo ""
   echo "Syntax:"
   echo " #   recherche une source par la destination :"
   echo "            search-com.sh -d numero -f file"
   echo " #   recherche une destination par la source :"
   echo "            search-com.sh -s numero -f file"
   echo " #   recherche une trace de communication complete :"
   echo "            search-com.sh -c com -f file"
   echo ""
   echo ""
   echo "options:"
   echo "-d    destination de l'appel sur 4-9 ou 10 chiffres en fonction du trunk"
   echo "-s    numéro de l'appelant ayant passé l'appel"
   echo "-f    full dans lequel chercher"
   echo "-c    recherche l'entiereté de la communication"
   echo "-h    affiche l'aide"
   echo
}



# Get the options
while getopts ":c:f:d:s:h" option; do
   case $option in
      f) #fichier de logs à rechercher
                f=${OPTARG}
                ;;
      d) #recherche la destination de la source
                d=${OPTARG}
                 ;;
      s) # recherche la source
                s=${OPTARG}
                ;;
      c) #affichage de la com
                c=${OPTARG}
                ;;
      h|*)
         usage
         exit;;

   esac
done


if [ -n "${d}" ] && [ -z "${f}" ]; then
                usage
elif [ -n "${s}" ] && [ -z "${f}" ]; then
                usage
elif [ -n "${c}" ] && [ -z "${f}" ]; then
                usage
fi

if [ -n "${d}" ]; then
        recherche_source $d $f
elif [ -n "${s}" ]; then
        recherche_destination $s $f
elif [ -n "${c}" ];then
        affiche_com $c $f
fi
