# SEARCH-COM

   Solution de recherche de logs dans les fichiers de logs asterisks sur xivo et wazo

# Principe 
   
   Recherche des communications passés en recherchant la source ou la destination d'un appel dans les fichiers de logs asterisk présent dans '/var/log/asterisk/'

# Fontionnement 


`
fonctions de recherche de numéros dans les logs

Syntax:
 #   recherche une source par la destination :
            search-com.sh -d numero -f file
 #   recherche une destination par la source :
            search-com.sh -s numero -f file
 #   recherche une trace de communication complete :
            search-com.sh -c com -f file

options:
-d    destination de l'appel sur 4-9 ou 10 chiffres en fonction du trunk
-s    numéro de l'appelant ayant passé l'appel
-f    full dans lequel chercher
-c    recherche l'entiereté de la communication
-h    affiche l'aide
`

# Exemples

## recherche d'une communication avec la destination 

`# search-com -d 123456789 -f full`
## recherche d'une communication avec la source

`# search-com -s 0123456789 -f full.1.gz`
## affichage de la trace d'appel

`# search-com -c C-000001234 -f full*`


# Installation 

## Copie Raw.

sélectionnez le fichier search-com.sh.
afficher la raw et copiez le code dans un fichier search-com.sh sur le serveur visé

## récupérer le git

sur le serveur visé :
- aller dans le dossier /opt/
- cloner le projet.
`git clone https://gitlab.com/satoss1861/search-com.git`

## ajouter un alias pour utiliser search-com sur le serveur.

- ouvez le fichier /root/.bashrc
- ajouter à la fin du fichier (ou après les alias existants):
    `alias search-com='sh /chemin/relatif/search-com.sh`   
(par exemple `alias search-com='sh /opt/search-com/search-com.sh`  ou `alias search-com='sh /root/search-com.sh`)
- rechargez le fichier .bashrc pour prendre en compte l'alias  `source .bashrc`


# TODO

- [ ] pb avec l'affichage du fichier dans la colonne 1 lors d'une recherche dans un seul fichier
- [ ] améliorer l'affichage du résultat
- [ ] faire un fichier d'install de l'alias pour automatisation (?) ou rajouter le /opt dans le $PATH (?)
